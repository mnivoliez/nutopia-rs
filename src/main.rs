extern crate specs;
#[macro_use]
extern crate specs_derive;

use specs::{
    Builder, Component, DispatcherBuilder, ReadStorage, RunNow, System, VecStorage, World,
    WriteStorage,
};

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct People {
    count: i32,
}

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct Nuts {
    count: f32,
}

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct Faith {
    count: f32,
}

struct FaithSystem;

impl<'a> System<'a> for FaithSystem {
    type SystemData = WriteStorage<'a, Faith>;

    fn run(&mut self, mut faith: Self::SystemData) {
        use specs::Join;

        for faith in (&mut faith).join() {
            faith.count -= 0.5;
        }
    }
}

struct ShowScore;

impl<'a> System<'a> for ShowScore {
    type SystemData = (
        ReadStorage<'a, People>,
        ReadStorage<'a, Nuts>,
        ReadStorage<'a, Faith>,
    );

    fn run(&mut self, (people, nuts, faith): Self::SystemData) {
        use specs::Join;

        for (people, nuts, faith) in (&people, &nuts, &faith).join() {
            println!("{:?}\n{:?}\n{:?}", &people, &nuts, &faith);
        }
    }
}
pub fn main() {
    let mut world = World::new();
    world.register::<People>();
    world.register::<Nuts>();
    world.register::<Faith>();

    let score = world
        .create_entity()
        .with(People { count: 10 })
        .with(Nuts { count: 10. })
        .with(Faith { count: 100. })
        .build();

    let mut dispatcher = DispatcherBuilder::new()
        .with(FaithSystem, "faith_system", &[])
        .with(ShowScore, "show_score", &["faith_system"])
        .build();

    dispatcher.dispatch(&mut world.res);
    world.maintain();
}
